target = "xilinx"
action = "simulation"
sim_tool = "modelsim"
top_module = "main"
syn_device = "XC6VLX240T"
fetchto = "../../ip_cores"
vlog_opt = "+incdir+../../sim +incdir+../../sim/wr-hdl"

include_dirs = [ "../../sim", "../../sim/wr-hdl" ]

files = [ "main.sv" ]

modules ={"local" : ["../../ip_cores/general-cores",
                     "../../modules/wrsw_pstats",
                     "../../modules/wrsw_pstats/wrsw_dummy"] };
