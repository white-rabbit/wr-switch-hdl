target = "xilinx"
action = "simulation"
sim_tool = "modelsim"
top_module = "main"
syn_device = "XC6VLX240T"
fetchto = "../../../ip_cores"
#vlog_opt = "+incdir+../../../sim +incdir+../../../ip_cores/general-cores/sim +incdir+../../../ip_cores/wr-cores/sim"

files = [ "main.sv" ]

include_dirs = [ "../../../sim", "../../../sim/wr-hdl" ]

modules = { "local" : [ "../../../modules/wrsw_swcore/mpm" ],
   					"git" : "git://ohwr.org/hdl-core-lib/general-cores.git::proposed_master"
}
					

					
