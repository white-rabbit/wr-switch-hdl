target = "xilinx" #  "altera" # 
action = "simulation"
sim_tool = "modelsim"
top_module = "main"
syn_device = "XC6VLX240T"

files = [ "tru.sv" ]
        
vlog_opt = "+incdir+../../sim +incdir+../../sim/wr-hdl"

include_dirs = [ "../../sim", "../../sim/wr-hdl" ]
        
modules = {"local": 
                [
                 "../../modules/wrsw_tru",
                 "../../ip_cores/general-cores/modules/genrams/"
                ],
             }
